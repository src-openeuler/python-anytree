%global modname anytree

Name:           python-anytree
Version:        2.12.1
Release:        1
Summary:        Powerful and Lightweight Python Tree Data Structure
License:        Apache-2.0
URL:            https://pypi.io/project/anytree
Source0:        %pypi_source %{modname}

BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-poetry-core
BuildRequires:  python3-pip
BuildRequires:  python3-wheel

%description
Powerful and Lightweight Python Tree Data Structure with various plugins.


%package -n python3-anytree
Summary:        Powerful and Lightweight Python Tree Data Structure

%description -n python3-anytree
Powerful and Lightweight Python Tree Data Structure with various plugins.

%prep
%setup -q -n %{modname}-%{version}

%build
%pyproject_build


%install
%pyproject_install

%files -n python3-anytree
%{python3_sitelib}/%{modname}/
%{python3_sitelib}/%{modname}-%{version}*
%doc README.rst
%license LICENSE


%changelog
* Fri Feb 23 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 2.12.1-1
- Update package to version 2.12.1

* Mon Nov 13 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 2.12.0-1
- Update package to version 2.12.0

* Wed Aug 02 2023 zhangchenglin <zhangchenglin@kylinos.cn> - 2.9.0-1
- Update package to version 2.9.0

* Wed Jan 20 2021 wutao <wutao61@huawei.com> - 2.8.0-2
- Modify license information.

* Thu Jun 11 2020 Dillon Cheng <dillon.chen@turbolinux.com.cn> - 2.8.0-1
- build for openEuler
